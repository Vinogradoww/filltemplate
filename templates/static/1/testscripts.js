function get_tmpl_id_list() {
    let select = document.getElementById("fv-topics");
    while (select.firstChild) {
        select.removeChild(select.firstChild);
    }
    let promise = fetch('http://127.0.0.1:5001/GetIdTemplates');
    promise.then(
        response => {
            return response.text();
        }
    ).then(
        text => {
            text = text.replace("[", "")
            text = text.replace("]", "")
            let textlist = text.split(',')
            for (let i = 0; i < textlist.length; i++) {
                let option = document.createElement("option")
                option.value = "id" + String(textlist[i])
                //option.selected ="selected"
                option.innerHTML = String(textlist[i])
                select.appendChild(option)
            }
        });
    get_tmpl_inf();
    return 0
}
get_tmpl_id_list()


function get_tmpl_inf() {
    document.getElementById("processTemplInf").style.display="block";
    let id_sbora = $('#fv-topics option:selected').text();
    let promise = fetch('http://127.0.0.1:5001/GetTemplateInfId/' + id_sbora);
    promise.then(
        response => {
           if (response.ok){
                return response.text();
            }
            else {
                return "0"
            }
        }
    ).then(
        text => {
            if (text == "0") {
                document.getElementById("processTemplInf").style.display="none";
                document.getElementById("TemplateName").innerHTML = "Выберите id сбора";
                document.getElementById("TemplateName").style.color  = 'black';
            }
            else {
                document.getElementById("TemplateId").innerHTML = id_sbora;
                document.getElementById("TemplateId").style.color  = 'black';
                document.getElementById("TemplateName").innerHTML = text.slice(text.indexOf('name')+7, text.indexOf('size')-3);
                document.getElementById("TemplateName").style.color  = 'black';
                document.getElementById("TemplateSize").innerHTML = text.slice(text.indexOf('size')+6, text.indexOf('num_files_used_this_template')-2);
                document.getElementById("TemplateSize").style.color  = 'black';
                document.getElementById("TemplateNumFiles").innerHTML = text.slice(text.indexOf('num_files_used_this_template')+30, text.indexOf('num_var')-2);
                document.getElementById("TemplateNumFiles").style.color  = 'black';
                document.getElementById("TemplateAvgFiles").innerHTML = text.slice(text.indexOf('"avg_time":')+11, -1);
                document.getElementById("TemplateAvgFiles").style.color  = 'black';
                document.getElementById("processTemplInf").style.display="none";

                document.getElementById("refUrl").value = "http://10.117.4.64:3011/to-docx/" + id_sbora;
            }
        }
    );
}
//get_tmpl_inf();

$( "#fv-topics" )
  .change(function () {
    get_tmpl_inf();
  })
  .change();



async function SendTemplate() {
        document.getElementById("successTempl").style.display="none";
        document.getElementById("errorTempl").style.display="none";
        document.getElementById("processTempl").style.display="block";
        const _files = document.getElementById("addTempl").files;
        const id = document.getElementById("IdSbora").value;
        var data = new FormData();
        _files[0].name = id + "_" + _files[0].name
        $.each(_files, function (key, value) {
            data.append(_files[0].name, value); 
        });
        $.ajax({
            url: "http://127.0.0.1:5002/addTemplate/" + id,
            type: "POST",
            data: data,
            cache: false,
            dataType: "json",
            processData: false,
            contentType: false,
            success: function (respond, status, jqXHR) {
                // в respond вернется ответ сервера
                document.getElementById("processTempl").style.display="none";
                document.getElementById("successTempl").style.display="block";
                get_tmpl_inf();
            },
            error: function (jqXHR, status, errorThrown) {
                // если ответ сервера не 200
                document.getElementById("processTempl").style.display="none";
                document.getElementById("errorTempl").style.display="block";
                errTextTempl.innerHTML = status
            },
        });
        get_tmpl_id_list();
    }



jQuery(function(){
    $("#addJson").change(function(){ // событие выбора файла
        document.getElementById("successJson").style.display="none";
        document.getElementById("errorJson").style.display="none";
        document.getElementById("processJson").style.display="block";

        const _files = document.getElementById("addJson").files;
        var data = new FormData();
        var filename = _files[0].name.replace('.json', '.docx')
        $.each(_files, function (key, value) {
            data.append(filename, value); 
        });
        var req = new XMLHttpRequest();
        req.open("POST", "http://127.0.0.1:5002/uploadJson");
        req.responseType = "blob";
        req.onload = function (event) {
            if (this.status == 200){
                var blob = req.response;
                var link=document.createElement('a');
                link.href=window.URL.createObjectURL(blob);
                link.download = filename;
                link.click();
                document.getElementById("processJson").style.display="none";
                document.getElementById("successJson").style.display="block";
                $('form[name=addJson]').trigger('reset');
                get_tmpl_inf();
            } else {
                document.getElementById("processJson").style.display="none";
                document.getElementById("errorJson").style.display="block";
            }
        };
        req.send(data);
    });
  });


async function downloadTemplate() {
    var req = new XMLHttpRequest();
    req.open("GET", "http://127.0.0.1:5001/GetTemplateFile", true);
    req.responseType = "blob";
    req.onload = function (event) {
        var blob = req.response;
        var fileName =  document.getElementById("TemplateName").innerText
        var link=document.createElement('a');
        link.href=window.URL.createObjectURL(blob);
        link.download = fileName;
        link.click();
    };
    if (document.getElementById("TemplateName").textContent != 'Шаблон не задан, добавьте новый шаблон!'){
        req.send();
    }
}      


myButton.addEventListener('click', function() {
    document.getElementById("SaveFile").classList.add("disabled")
    document.getElementById("processFileInf").style.display="block";
    let id = document.getElementById("default-01").value;
    let promise = fetch('http://127.0.0.1:5001/GetFileInformation?q=' + id);
    promise.then(
        response => {
            return response.text();
        }
    ).then(
        text => {
            resId.innerHTML = text.slice(text.indexOf('id')+4, text.indexOf('filename')-2);
            resId.style.color  = 'black';
            resFilename.innerHTML = text.slice(text.indexOf('filename')+15, text.indexOf('date')-3);
            resFilename.style.color = 'black';
            resDateid.innerHTML = text.slice(text.indexOf('date')+7, text.indexOf('timestart')-3);
            resDateid.style.color = 'black';
            timestart.innerHTML = text.slice(text.indexOf('timestart')+12, text.indexOf('exe_time')-3);
            timestart.style.color = 'black';
            exe_time.innerHTML = text.slice(text.indexOf('exe_time')+10, text.indexOf('size')-2);
            exe_time.style.color = 'black';
            size.innerHTML = text.slice(text.indexOf('size')+6, text.indexOf('json_name')-2);
            size.style.color = 'black';
            json_name.innerHTML = text.slice(text.indexOf('json_name')+12, text.indexOf('template_name')-3);
            json_name.style.color = 'black';
            template_name.innerHTML = text.slice(text.indexOf('template_name')+16, text.indexOf('num_var')-3);
            template_name.style.color = 'black';
            num_var.innerHTML = text.slice(text.indexOf('num_var')+9, text.indexOf('num_pass_var')-2);
            num_var.style.color = 'black';
            num_pass_var.innerHTML = text.slice(text.indexOf('num_pass_var')+14, -1);
            num_pass_var.style.color = 'black';
            withUnit1.style.color = 'black';
            withUnit2.style.color = 'black';
            document.getElementById("processFileInf").style.display="none";
            document.getElementById("SaveFile").classList.remove("disabled");
        }
    );
});


async function A() { 
    document.getElementById("myTable").style.display="block";  // Показать таблицу 
}

async function B() { 
    document.getElementById("myTable").style.display="none";   //Скрыть
}


async function downloadResFile() {
    var req = new XMLHttpRequest();
    req.open("GET", "http://127.0.0.1:5001/GetResFile?name="+ document.getElementById("resFilename").innerText, true);
    req.responseType = "blob";
    req.onload = function (event) {
        var blob = req.response;
        var fileName =  document.getElementById("resFilename").innerText
        var link=document.createElement('a');
        link.href=window.URL.createObjectURL(blob);
        link.download = fileName;
        link.click();
    };
    req.send();
}