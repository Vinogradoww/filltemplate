from database import Base
from sqlalchemy import Column, Integer, String, Date, DateTime, Float, ForeignKey
from database import ENGINE

class DatesTable(Base):
    __tablename__ = 'dates'
    id = Column(Integer, primary_key=True, autoincrement=True)
    date = Column(Date, nullable=False)
    numfiles = Column(Integer)
    fulltime = Column(Integer)
    avgtime = Column(Float)


class FileTable1(Base):
    __tablename__ = 'files1'
    id = Column(Integer, primary_key=True, autoincrement=True)
    id_sbora = Column(Integer)      #id сбора
    filename = Column(String(255), nullable=False)      #имя сформированного документа
    dateid = Column(Integer, ForeignKey("dates.id"))
    timestart = Column(String(50), nullable=False)
    exe_time = Column(Integer)      #время выполнения в секундах
    size = Column(Integer)          #размер файла в байтах
    json_name = Column(String(255))
    template_name = Column(String(255))

    def prnt(self):
        print(self.id, '\t', self.filename)


class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True, autoincrement=True)
    login = Column(String(255), nullable=False)
    hashed_password = Column(String(255), nullable=False)
    
    

def main():
    Base.metadata.create_all(bind=ENGINE)

if __name__ == "__main__":
    main()
