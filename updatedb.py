from datetime import datetime, date
from sqlalchemy.orm import Session
import model
import time
import os

from passlib.context import CryptContext



def get_password_hash(password):
    pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
    return pwd_context.hash(password)

def add_user(db: Session, login = "admin", password = "admin"):
    hashed_password = get_password_hash(password)
    login_exists = db.query(model.User).filter(model.User.login == login).first()
    password_exists = db.query(model.User).filter(model.User.hashed_password == hashed_password).first()
    if not login_exists:
        if not password_exists:
            user = model.User(login = login, hashed_password = hashed_password)
            db.add(user)
            db.commit()
            return user
        else:
            return "password_exists"
    else:
        return "login_exists"

def get_user(db: Session, login):
    user = db.query(model.User).filter(model.User.login == login).first()
    if user:
        return {"id": user.id, "username": login, "hashed_password": user.hashed_password, "disabled": False,}

def change_password(db: Session, id, new_password):
    hashed_password = get_password_hash(new_password)
    user = db.query(model.User).filter(model.User.id == id).first()
    password_exists = db.query(model.User).filter(model.User.hashed_password == hashed_password).first()
    if not password_exists:
        user.hashed_password = hashed_password
        db.commit()
        return user
    else:
        return "password_exists"

def del_user(db: Session, id):
    user = db.query(model.User).filter(model.User.id == id).delete()
    db.commit()
    return



async def add(db: Session, resultfile, idsbora, exetime, jsonname, templatename):
    curDate = date.today()
    Date_query = db.query(model.DatesTable).filter(model.DatesTable.date == curDate).first()
    if Date_query == None:
        Date_table = model.DatesTable(date = curDate, numfiles = 1, fulltime = exetime, avgtime = exetime)
        db.add(Date_table)
        db.commit()
    else:
        Date_query.numfiles =  Date_query.numfiles + 1
        Date_query.fulltime = Date_query.fulltime + exetime
        Date_query.avgtime = Date_query.fulltime/Date_query.numfiles
        db.commit()
    Date_query = db.query(model.DatesTable).filter(model.DatesTable.date == curDate).first()
    FI_table = model.FileTable1(filename = resultfile, id_sbora = idsbora, dateid = Date_query.id, timestart = datetime.now().strftime("%H:%M:%S"), exe_time = exetime, 
        size = os.path.getsize(resultfile), json_name = jsonname, template_name = templatename)
    db.add(FI_table)
    db.commit()
    return FI_table

async def get_templ(db: Session, filename):
    count_files = db.query(model.FileTable1).filter(model.FileTable1.template_name == filename).count()
    files_size = db.query(model.FileTable1.size).filter(model.FileTable1.template_name == filename).all()
    files_time = db.query(model.FileTable1.exe_time).filter(model.FileTable1.template_name == filename).all()
    avg_time = 0
    for f in files_time:
        avg_time += f[0]
    try:
        avg_time = avg_time/count_files
    except:
        avg_time = 0.0
    return (db.query(model.FileTable1).filter(model.FileTable1.template_name == filename).count(), avg_time)

async def get_file(db: Session, Id):
    if (Id <= db.query(model.FileTable1).count()) & (Id != 0 ):
        res = db.query(model.FileTable1).filter(model.FileTable1.id == Id).first()
        res_date = db.query(model.DatesTable).filter(model.DatesTable.id == res.dateid).first()
        return (res, res_date.date)
    elif Id == 0:
        res = db.query(model.FileTable1).all()[-1]
        res_date = db.query(model.DatesTable).filter(model.DatesTable.id == res.dateid).first()
        return (res, res_date.date)
    else:
        return (None, None)

async def gel_all_files(db: Session, month: str):
    month = month.split(" ")
    month_replace = {"Январь": "01", "Ферваль": "02", "Март": "03", 
                    "Апрель": "04", "Май": "05", "Июнь": "06", 
                    "Июль": "07", "Август": "08", "Сентябрь": "09",
                    "Октябрь": "10","Ноябрь": "11", "Декабрь": "12"}
    month[0] = month_replace[month[0]]     
    date = month[1] + "-" + month[0]

    date_query = db.query(model.DatesTable).all()
    date_ids = []
    for i in date_query:
        if str(i.date)[:7] == date:
            date_ids.append(i.id)

    files = db.query(model.FileTable1).filter(model.FileTable1.dateid.in_(date_ids)).all()
    arr = []

    for f in files:
        fdate = db.query(model.DatesTable).filter(model.DatesTable.id == f.dateid).first()
        if date == str(fdate.date)[:7]:
            tmpd = {"id_sbora": "", "id_file": "", "templ_name": "", "file_name": "", "file_date_update": "", "file_size": ""}
            tmpd["id_sbora"] = str(f.id_sbora)
            tmpd["id_file"] = str(f.id)
            tmpd["templ_name"] = str(f.template_name)
            tmpd["file_name"] = str(f.filename)[4:]
            tmpd["file_date_update"] = str(fdate.date) + ' ' + str(f.timestart)
            if f.size > 1024:
                kbsize = round(f.size/1024)
            tmpd["file_size"] = str(kbsize)
            arr.append(tmpd)
    return list(reversed(arr))

async def get_stat(db: Session):
    data_days = db.query(model.DatesTable).all()
    if len(data_days) > 7: data_days = data_days[-7:]
    
    data_month = db.query(model.DatesTable).all()
    month = [str(d.date)[:7] for d in data_month]
    numfiles = [d.numfiles for d in data_month]

    month_dict = dict.fromkeys(month, 0)
    for m in month_dict.keys():
        for i in range(len(month)):
            if m == month[i]:
                month_dict[m] += numfiles[i]
    month_replace = {"01": "Январь", "02": "Ферваль", "03": "Март", 
                    "04": "Апрель", "05": "Май", "06": "Июнь", 
                    "07": "Июль", "08": "Август", "09": "Сентябрь",
                    "10": "Октябрь","11": "Ноябрь", "12": "Декабрь"}
    month_dict_keys = list(month_dict.keys())
    if len(month_dict_keys) > 12: month_dict_keys = month_dict_keys[-12:]

    return {"days": [d.date for d in data_days], 
            "days_stat": [d.numfiles for d in data_days], 
            "month": [month_replace[m.split("-")[1]] + ' ' + m.split("-")[0] for m in month_dict_keys], 
            "month_stat": list(month_dict.values())}

if __name__ == '__main__':
    from database import SessionLocal
    #get(SessionLocal, 1, "2_Шаблон Форма мониторинга международной деятельности (2020) в.1.docx")
    