from fastapi import FastAPI, File, UploadFile, HTTPException, Form, Request, BackgroundTasks, Depends, status
from fastapi.security import OAuth2PasswordRequestForm, OAuth2PasswordBearer
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
from starlette.responses import FileResponse, JSONResponse
from starlette.requests import Request

from database import SessionLocal
import compilling
import updatedb
import schemas
import auth

from pathlib import Path
from loguru import logger
import logging
import uvicorn
import shutil
import time
import json
import os






class InterceptHandler(logging.Handler):
    def emit(self, record):
        try:
            level = logger.level(record.levelname).name
        except ValueError:
            level = record.levelno
        frame, depth = logging.currentframe(), 2
        while frame.f_code.co_filename == logging.__file__:
            frame = frame.f_back
            depth += 1
        logger.opt(depth=depth, exception=record.exc_info).log(
            level, record.getMessage()
        )
console_out = logging.StreamHandler()


if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=3011, log_level="info")
app = FastAPI(title="fill template")


logging.getLogger().handlers = [InterceptHandler(), console_out]
logger.configure(
    handlers=[{"sink": "log.log", "level": logging.DEBUG}]
)
logging.getLogger("uvicorn.access").handlers = [InterceptHandler(), console_out]


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def make_dirs():
    # шаблоны
    tpl_path = Path() / 'tpl'
    tpl_all_path = tpl_path / 'all'
    if not tpl_path.is_dir():
        tpl_path.mkdir()
        if not tpl_all_path.is_dir():
            tpl_all_path.mkdir()
    # резлультаты
    res_path = Path() / 'res'
    if not res_path.is_dir():
        res_path.mkdir()

    # json 
    json_path = Path() / 'json'
    if not json_path.is_dir():
        json_path.mkdir()
make_dirs()



@app.post('/addTemplate/{id_sbora}')
async def uploadT(req: Request, id_sbora = 0):
    tpl = Path() / 'tpl'
    tpl_all = tpl / 'all'
    tpl_id_sbora = tpl / f'{id_sbora}'
    if not tpl_id_sbora.is_dir():
        tpl_id_sbora.mkdir()
    else:
        for file in tpl_id_sbora.glob("*.*"):
            file.unlink()
    try:
        data = (await req.form())._dict
        file = data[list(data.keys())[0]]
        file_path = tpl_id_sbora / file.filename
        with file_path.open("wb") as buffer:
            shutil.copyfileobj(file.file, buffer)
        shutil.copyfile(file_path, tpl_all / file_path.name)
        return JSONResponse(status_code = 200, content = {"template saved": file_path.name})
    except:
        raise HTTPException(status_code = 404, detail = "Check template or request")


def save_json(data, tstart, id_sbora):
    _json = Path() / 'json'
    json_path = _json / f'data_{round(tstart)}_id_{id_sbora}.json'
    with open(json_path, 'w') as fp:
        json.dump(data, fp)


@app.post('/to-docx/{id_sbora}')
async def to_docx(background_tasks: BackgroundTasks, req: Request, id_sbora = 0):
    tstart = time.time()
    json_path = Path() / 'json' / f'data_{round(tstart)}_id_{id_sbora}.json'
    tpl_path = Path() / 'tpl' / f'{id_sbora}'
    tpl_path = next(Path(tpl_path).glob('*'))
    res_path = Path() / 'res'
    logger.info("start {}", f"{json_path} id {id_sbora}")    
        
    data = (await req.json())
    
    background_tasks.add_task(save_json, data, tstart, id_sbora)

    message, exetime, res_path = await compilling.main(data, id_sbora, tstart, tpl_path, res_path)

    if message == 1:
        try:
            session = SessionLocal()
            res = await updatedb.add(session, res_path, id_sbora, exetime, json_path, tpl_path.name)
            session.close()
        except Exception as exc:
            print(1)
            logger.exception(exc)
            with open("infToDb.txt", 'a') as f:
                f.write(f"{res_path}, {id_sbora}, {exetime}, {json_path}, {tpl_path.name}")
            #bot.f2(res_path)
        finally:
            print(2)
            logger.success("ready {}", res_path)
            return FileResponse(res_path, filename = res_path.name)
    else:
        print(message)
        raise HTTPException(status_code = 404, detail = message[message.find('%')+1:message.find('$')])



@app.get("/GetTemplateInfId/{id_sbora}", response_model=schemas.TemplateOutput)
async def get_tmpl_inf(id_sbora):
    if not os.path.exists(f'tpl/{id_sbora}/'):
        return schemas.TemplateOutput(name = "Шаблон не задан, добавьте новый шаблон!", size = 0, 
        num_files_used_this_template = 0, num_var = 0, avg_time = 0)
    template = os.listdir(f'tpl/{id_sbora}/')[0]
    doc_path = f'tpl/{id_sbora}/' + template

    #tpl_path = Path() / 'tpl' / f'{id_sbora}'
    #tpl_path = next(Path(tpl_path).glob('*'))
    nv = compilling.numvar(doc_path)   #количество переменных в шаблоне
    size = os.path.getsize(f'tpl/{id_sbora}/'+template)
    if size > 1024:
        size = round(size/1024)
    session = SessionLocal()
    num_files, avgtime = await updatedb.get_templ(session, template)
    session.close()
    return schemas.TemplateOutput(name = template, size = size, 
        num_files_used_this_template = num_files, num_var = nv, avg_time = round(avgtime, 2))



@app.get("/GetIdTemplates")
async def get_id_templates():
    ids = dict.fromkeys(os.listdir('tpl/'))
    for key in ids.keys():
        ids[key] = os.path.getctime(f'tpl/{key}')
    ids = list(ids.items())
    ids.sort(key=lambda i: i[1])
    res = []
    for i in ids:
        try:
            res.append(int(i[0]))
        except:
            pass
    return res
    
    

@app.get("/GetTemplateFile/{id_sbora}")
async def get_Template_file(id_sbora):
    files = os.listdir(f'tpl/{id_sbora}/')
    return FileResponse(f'tpl/{id_sbora}/'+os.listdir(f'tpl/{id_sbora}/')[0], filename = os.listdir(f'tpl/{id_sbora}/')[0])



@app.get("/GetFileInformation", 
    description = "Get information about file with the entered id; to get information about last file enter 0", 
    response_model=schemas.FileOutput)
async def get_File_inf(id: int = None, q: str = None):
    if q:
        id = int(q)
    session = SessionLocal()
    query, fdate = await updatedb.get_file(session, id)
    session.close()
    if query is None:
        raise HTTPException(status_code=404, detail="Wrong ID")
    nv, jsnv = compilling.numvar('tpl/all/'+query.template_name, query.json_name)
    if query.size > 1024:
        kbsize = round(query.size/1024)
    return  schemas.FileOutput(id = query.id, id_sbora = query.id_sbora, filename = query.filename, date = str(fdate), timestart = query.timestart, 
        exe_time = query.exe_time, size = kbsize, json_name = query.json_name, template_name = query.template_name, num_var = nv, num_pass_var = jsnv)



@app.get("/GetResFile")
async def get_Res_file(name: str):
    if os.path.exists(f'res/{name}'):
        return FileResponse(f'res/{name}', filename = name)
    return "0"



@app.get("/DeleteTemplate")
async def Delete_Template(id_sbora):
    if os.path.exists(f'tpl/{id_sbora}'):
        for file in os.listdir(f'tpl/{id_sbora}/'):
            os.remove(f'tpl/{id_sbora}/'+file)
        os.rmdir(f'tpl/{id_sbora}')
    return 200


@app.post("/AllFiles")
async def get_All_files(month: str = Form(...)):
    session = SessionLocal()
    files = await updatedb.gel_all_files(session, month)
    session.close()
    return {"all_files_list": files}


@app.get("/getStat")
async def get_Stat():
    session = SessionLocal()
    stat = await updatedb.get_stat(session) # количество обработанных файлов по дням
    session.close()
    return stat


app.mount("/static", StaticFiles(directory = "static"), name="static")
templates = Jinja2Templates(directory = "templates")

@app.get("/page", response_class = HTMLResponse)
async def read_item(request: Request):#, current_user: schemas.User = Depends(auth.get_current_active_user)):
    return templates.TemplateResponse("auth.html", {"request": request})

@app.get("/main_page", response_class = HTMLResponse)
async def read_item(request: Request):#, current_user: schemas.User = Depends(auth.get_current_active_user)):
    return templates.TemplateResponse("index.html", {"request": request})



@app.post('/uploadJson/{id_sbora}') # загрузка json файла с панели управления
async def uploadJfile(background_tasks: BackgroundTasks, id_sbora = 0, file: UploadFile = File(...)):
    json_path = Path() / 'json' /  file.filename
    tstart = time.time()
    #json_path = Path() / 'json' / f'data_{round(tstart)}_id_{id_sbora}.json'
    tpl_path = Path() / 'tpl' / f'{id_sbora}'
    tpl_path = next(Path(tpl_path).glob('*'))
    res_path = Path() / 'res'
    logger.info("start {}", f"{json_path} id {id_sbora}")    
        
    data = json.loads(file.file.read())

    background_tasks.add_task(save_json, data, tstart, id_sbora)

    message, exetime, res_path = await compilling.main(data, id_sbora, tstart, tpl_path, res_path)

    if message == 1:
        try:
            session = SessionLocal()
            res = await updatedb.add(session, res_path, id_sbora, exetime, json_path, tpl_path.name)
            session.close()
        except Exception as exc:
            print(1)
            logger.exception(exc)
            with open("infToDb.txt", 'a') as f:
                f.write(f"{res_path}, {id_sbora}, {exetime}, {json_path}, {tpl_path.name}")
            #bot.f2(res_path)
        finally:
            print(2)
            logger.success("ready {}", res_path)
            return FileResponse(res_path, filename = res_path.name)
    else:
        print(message)
        raise HTTPException(status_code = 404, detail = message[message.find('%')+1:message.find('$')])

    


@app.post("/adduser")
async def get_all_users(login: str, password: str):
    session = SessionLocal()
    user = updatedb.add_user(session, login = login, password = password)
    session.close()
    if type(user) != str:
        return JSONResponse(status_code = 200, content = f"Пользователь {login} успешно зарегестрирован!")
    else:
        return JSONResponse(status_code = 400, content = user)



@app.post("/token", response_model = schemas.Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = auth.authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail = "Incorrect username or password",
            headers = {"WWW-Authenticate": "Bearer"},
        )
    access_token = auth.create_access_token(data = {"sub": user.username})
    return {"access_token": access_token, "token_type": "bearer"}



@app.post("/change_password")
async def change_password(new_password: str = Form(...), current_user: schemas.User = Depends(auth.get_current_active_user)):
    session = SessionLocal()
    user = updatedb.change_password(session, id = current_user.id, new_password = new_password)
    session.close()
    if type(user) != str:
        return JSONResponse(status_code = 200, content = f"Пароль для пользователя {current_user.username} успешно изменен!")
    else:
        return JSONResponse(status_code = 400, content = user)



@app.post("/del_user")
async def del_user(current_user: schemas.User = Depends(auth.get_current_active_user)):
    session = SessionLocal()
    user = updatedb.del_user(session, id = current_user.id)
    session.close()
    return JSONResponse(status_code = 200, content = f"Пользователь {current_user.username} удалён")

