from docxtpl import DocxTemplate
from fastapi import UploadFile, BackgroundTasks
from docx import Document 
from copy import deepcopy
import shutil
import time
import json
import re


def numvar(doc_path, file = None):
    doc = Document(doc_path)
    templ = re.compile(r'\{\{[ A-Za-z0-9_\.%]*\}\}')
    count = 0
    if file == None:
        for table in doc.tables:
            for row in table.rows:
                for cell in row.cells:
                    if re.search(templ, cell.text):
                        count += 1
        for p in doc.paragraphs:
            if re.search(templ, p.text):
                count += 1
        return count
    else:
        with open(file, "r") as f:
            f = f.read()
            data = {}
            c = json.loads(f)
            for k in c.keys():
                data.update(c[k])   
            jsonkeys = []
            for key in data.keys():
                jsonkeys.append(key)
                if type(data[key]) == 'list':
                    for d in data[key]:
                        for k in d.keys():
                            jsonkeys.append(k)
            jscount = 0
            keysintpl = []
            for table in doc.tables:
                for row in table.rows:
                    for cell in row.cells:
                        if re.search(templ, cell.text):
                            count += 1
                            keysintpl.append(cell.text.strip(r" {{}}"))
            for p in doc.paragraphs:
                if re.search(templ, p.text):
                    count += 1
                    keysintpl.append(p.text.strip(r" {{}}"))
            for k in keysintpl:
                if k.replace("item.", '') in jsonkeys:
                    jscount += 1
            return count, jscount



async def main(data, id_sbora, tstart, template, res): 
    context = {}
    try:
        for k in data.keys():
            context.update(data[k])         
        for key, val in context.items():
            if val == None:
                context[key] = ''
    except Exception as err:
        return (f"Error!\n{err}\n--------------\n%Check json$\n--------------\n", None, None)

    tpl = DocxTemplate(template)

    try:
        tpl.render(context)
    except Exception as err:
        return (f"Error!\n{err}\n---------------\n%Check template$\n---------------\n", None, None)
    
    res_path = res / f'res_{id_sbora}_{round(tstart)}.docx'
    tpl.save(res_path)
    exetime = time.time() - tstart
    return (1, exetime, res_path)



