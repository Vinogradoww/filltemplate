var first_select = 0;

function get_tmpl_id_list() {
    let select = document.getElementById("fv-topics");
    while (select.firstChild) {
        select.removeChild(select.firstChild);
    }
    let promise = fetch('http://127.0.0.1:3011/GetIdTemplates');
    promise.then(
        response => {
            return response.text();
        }
    ).then(
        text => {
            text = text.replace("[", "")
            text = text.replace("]", "")
            let textlist = text.split(',')
            for (let i = textlist.length-1; i>-1; i--) {
                let option = document.createElement("option")
                option.value = "id" + String(textlist[i])
                option.innerHTML = String(textlist[i])
                select.appendChild(option)
            }
            let flag = 1;
            window.first_select = textlist[0];
            get_tmpl_inf();
        });
    return flag
}
var flag = get_tmpl_id_list();


function get_tmpl_inf() {
    document.getElementById("DeleteTemplate").style.display="none";
    document.getElementById("processTemplInf").style.display="block";
    /*if (window.flag == 1) {
        var id_sbora = window.first_select;
        window.flag = 0;
    }
    else {
        var id_sbora = $('#fv-topics option:selected').text();
    }*/
    var id_sbora = $('#fv-topics option:selected').text();
    let promise = fetch('http://127.0.0.1:3011/GetTemplateInfId/' + id_sbora);
    promise.then(
        response => {
           if (response.ok){
                return response.json();
            }
            else {
                return "0"
            }
        }
    ).then(
        text => {
            //alert(text.num_var)
            if (text == "0") {
                document.getElementById("processTemplInf").style.display="none";
                document.getElementById("TemplateName").innerHTML = "Выберите id сбора";
                document.getElementById("TemplateName").style.color  = 'black';
            }
            else {
                document.getElementById("TemplateId").innerHTML = id_sbora;
                document.getElementById("TemplateId").style.color  = 'black';
                document.getElementById("TemplateName").innerHTML = text.name//slice(text.indexOf('name')+7, text.indexOf('size')-3);
                document.getElementById("TemplateName").style.color  = 'black';
                document.getElementById("TemplateSize").innerHTML = text.size//slice(text.indexOf('size')+6, text.indexOf('num_files_used_this_template')-2);
                document.getElementById("TemplateSize").style.color  = 'black';
                document.getElementById("TemplateNumFiles").innerHTML = text.num_files_used_this_template//slice(text.indexOf('num_files_used_this_template')+30, text.indexOf('num_var')-2);
                document.getElementById("TemplateNumFiles").style.color  = 'black';
                document.getElementById("TemplateAvgFiles").innerHTML = text.avg_time//slice(text.indexOf('"avg_time":')+11, -1);
                document.getElementById("TemplateAvgFiles").style.color  = 'black';
                document.getElementById("TemplateNumVar").innerHTML = text.num_var
                document.getElementById("TemplateNumVar").style.color  = 'black';

                document.getElementById("processTemplInf").style.display="none";
                document.getElementById("ViewTemplate").style.display="block";
                document.getElementById("DeleteTemplate").style.display="block";

                document.getElementById("refUrl").value = "http://127.0.0.1:3011/to-docx/" + id_sbora;
            }
        }
    );
}
//get_tmpl_inf();

/*$( "#fv-topics" )
  .change(function () {
    get_tmpl_inf();
  })
  .change();*/
  
  
document.getElementById("fv-topics").addEventListener('change', function () {
  get_tmpl_inf();
})



async function SendTemplate() {
        document.getElementById("successTempl").style.display="none";
        document.getElementById("errorTempl").style.display="none";
        document.getElementById("processTempl").style.display="block";
        const _files = document.getElementById("addTempl").files;
        const id = document.getElementById("IdSbora").value;
        var data = new FormData();
        _files[0].name = id + "_" + _files[0].name
        $.each(_files, function (key, value) {
            data.append(_files[0].name, value); 
        });
        $.ajax({
            url: "http://127.0.0.1:3011/addTemplate/" + id,
            type: "POST",
            data: data,
            cache: false,
            dataType: "json",
            processData: false,
            contentType: false,
            success: function (respond, status, jqXHR) {
                // в respond вернется ответ сервера
                document.getElementById("processTempl").style.display="none";
                document.getElementById("successTempl").style.display="block";
                get_tmpl_id_list();
                setTimeout(() => { 
                    document.getElementById("successTempl").style.display="none";
                    document.getElementById("IdSbora").value = '';
                    }, 1000);
            },
            error: function (jqXHR, status, errorThrown) {
                // если ответ сервера не 200
                document.getElementById("processTempl").style.display="none";
                document.getElementById("errorTempl").style.display="block";
                errTextTempl.innerHTML = status
            },
        });
        get_tmpl_id_list();
    }


jQuery(function(){
    $("#addJson").change(function(){ // событие выбора файла
        document.getElementById("successJson").style.display="none";
        document.getElementById("errorJson").style.display="none";
        document.getElementById("processJson").style.display="block";

        const _files = document.getElementById("addJson").files;
        let data = new FormData();
        let filename = _files[0].name.replace('.json', '.docx')
        //alert(filename)
        $.each(_files, function (key, value) {
            data.append(filename, value); 
        });
        let id_sbora = $('#fv-topics option:selected').text();
        let req = new XMLHttpRequest();
        //alert(id_sbora)
        
        req.open("POST", "http://127.0.0.1:3011/uploadJson/" + id_sbora);
        req.responseType = "blob";
        req.onload = function (event) {
            if (this.status == 200){
                let blob = req.response;
                let link=document.createElement('a');
                link.href=window.URL.createObjectURL(blob);
                link.download = filename;
                link.click();
                document.getElementById("processJson").style.display="none";
                document.getElementById("successJson").style.display="block";
                $('form[name=addJson]').trigger('reset');//очистка формы от файла ???
                get_tmpl_inf();

                //render_files_table();

            } else {
                document.getElementById("processJson").style.display="none";
                document.getElementById("errorJson").style.display="block";
            }
        };
        req.send(data);
    });
  });


async function downloadTemplate() {
	let id_sbora = $('#fv-topics option:selected').text();
    var req = new XMLHttpRequest();
    req.open("GET", "http://127.0.0.1:3011/GetTemplateFile/"  + id_sbora, true);
    req.responseType = "blob";
    req.onload = function (event) {
        var blob = req.response;
        var fileName =  document.getElementById("TemplateName").innerText
        var link=document.createElement('a');
        link.href=window.URL.createObjectURL(blob);
        link.download = fileName;
        link.click();
    };
    if (document.getElementById("TemplateName").textContent != 'Шаблон не задан, добавьте новый шаблон!'){
        req.send();
    }
}      


myButton.addEventListener('click', function() {
    document.getElementById("btn_resFilename").classList.add("disabled")
    document.getElementById("processFileInf").style.display="block";
    let id = document.getElementById("default-01").value;
    let promise = fetch('http://127.0.0.1:3011/GetFileInformation?q=' + id);
    promise.then(
        response => {
            return response.text();
        }
    ).then(
        text => {
            resId.innerHTML = text.slice(text.indexOf('id')+4, text.indexOf('filename')-2);
            resId.style.color  = 'black';
            resFilename.innerHTML = text.slice(text.indexOf('filename')+15, text.indexOf('date')-3);
            resFilename.style.color = 'black';
            resDateid.innerHTML = text.slice(text.indexOf('date')+7, text.indexOf('timestart')-3);
            resDateid.style.color = 'black';
            timestart.innerHTML = text.slice(text.indexOf('timestart')+12, text.indexOf('exe_time')-3);
            timestart.style.color = 'black';
            exe_time.innerHTML = text.slice(text.indexOf('exe_time')+10, text.indexOf('size')-2);
            exe_time.style.color = 'black';
            size.innerHTML = text.slice(text.indexOf('size')+6, text.indexOf('json_name')-2);
            size.style.color = 'black';
            json_name.innerHTML = text.slice(text.indexOf('json_name')+12, text.indexOf('template_name')-3);
            json_name.style.color = 'black';
            template_name.innerHTML = text.slice(text.indexOf('template_name')+16, text.indexOf('num_var')-3);
            template_name.style.color = 'black';
            num_var.innerHTML = text.slice(text.indexOf('num_var')+9, text.indexOf('num_pass_var')-2);
            num_var.style.color = 'black';
            num_pass_var.innerHTML = text.slice(text.indexOf('num_pass_var')+14, -1);
            num_pass_var.style.color = 'black';
            withUnit1.style.color = 'black';
            withUnit2.style.color = 'black';
            document.getElementById("processFileInf").style.display="none";
            document.getElementById("btn_resFilename").classList.remove("disabled");
        }
    );
});


async function A() { 
    document.getElementById("myTable").style.display="block";  // Показать таблицу 
}

async function B() { 
    document.getElementById("myTable").style.display="none";   //Скрыть
}


async function downloadResFile(id) {
    filename = document.getElementById(id.slice(4)).innerText
    var req = new XMLHttpRequest();
    req.open("GET", "http://127.0.0.1:3011/GetResFile?name="+ filename, true);
    req.responseType = "blob";
    req.onload = function (event) {
        var blob = req.response;
        var link=document.createElement('a');
        link.href=window.URL.createObjectURL(blob);
        link.download = filename;
        link.click();
    };
    req.send();
}


function DeleteTemplate() {
    let id_sbora = $('#fv-topics option:selected').text();
    let promise = fetch('http://127.0.0.1:3011/DeleteTemplate?id_sbora=' + id_sbora);
	get_tmpl_id_list();
}


function auth() {
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;
    console.log(username, password)
    var data = new FormData();
    data.append("username", username)
    data.append("password", password)
    $.ajax({
        url: "http://127.0.0.1:3011/token",
        type: "POST",
        data: data,
        cache: false,
        dataType: "json",
        processData: false,
        contentType: false,
        success: function (respond, status, jqXHR) {
            // в respond вернется ответ сервера
            let token = respond["access_token"]
            console.log(token)
            document.location.href = "http://127.0.0.1:3011/main_page"
        },
        error: function (jqXHR, status, errorThrown) {
            // если ответ сервера не 200
            console.log(status)
        },
    });
}


function get_day_stat() {
    //let promise = fetch("http://10.117.4.64:3011/getStat");
    let promise = fetch("http://127.0.0.1:3011/getStat");
    promise.then(
        response => {
            return response.json();
        }
    ).then(
        json => {
            var chLine = document.getElementById("chLine");
            var chartData = {
                labels: json.days,
                datasets: [{
                    data: json.days_stat,
                    backgroundColor: 'transparent',
                    borderColor: '#6576ff',
                    borderWidth: 4,
                    pointBackgroundColor: '#6576ff'
                }]
            };

            if (chLine) {
                new Chart(chLine, {
                type: 'line',
                data: chartData,
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: false
                            }
                        }]
                    },
                    legend: {
                        display: false
                    }
                }
                });
            }

            var chBar = document.getElementById("chBar");
            var chartData = {
                labels: json.month,
                datasets: [{
                    data: json.month_stat,
                    backgroundColor: '#6576ff'
                }]
            };

            if (chBar) {
                new Chart(chBar, {
                type: 'bar',
                data: chartData,
                options: {
                    scales: {
                        xAxes: [{
                            barPercentage: 0.4,
                            categoryPercentage: 0.5
                        }],
                        yAxes: [{
                            ticks: {
                                beginAtZero: false
                            }
                        }]
                    },
                    legend: {
                        display: false
                    }
                }
                });
            }
        }
    );        
}
get_day_stat();


function get_month_select() {
    let select = document.getElementById("select_month");
    while (select.firstChild) {
        select.removeChild(select.firstChild);
    }
    var flag_month = 1;
    var first_select_month = '';
    let promise = fetch("http://127.0.0.1:3011/getStat");
    promise.then(
        response => {
            return response.json();
        }
    ).then(
        json => {
            var month = json.month;
            for (let i = month.length-1; i>-1; i--) {
                let option = document.createElement("option")
                option.value = "month_" + String(month[i])
                option.innerHTML = String(month[i])
                select.appendChild(option)
            }
            flag_month = 1;
            first_select_month = month[month.length-1];
            render_files_table(flag_month, first_select_month);
        }
    );  
}
get_month_select();


function pages(){
    nav = document.getElementById("filesnav");
    nav.innerHTML = '';
    var rowsShown = 300;
    var rowsTotal = $('#filesTable tbody tr').length;
    var numPages = rowsTotal/rowsShown;
    for(i = 0; i  < numPages; i++) {
        var pageNum = i + 1;
        $('#filesnav').append('<li class="page-item"><a class="page-link" href="#" rel="'+i+'">'+pageNum+'</a></li>');
    }
    $('#filesTable tbody tr').hide();
    $('#filesTable tbody tr').slice(0, rowsShown).show();
    '<li class="page-item active" aria-current="page"><a class="page-link" href="#">2 <span class="sr-only">(current)</span></a></li>'

    $('#filesnav li:first').addClass('active');
    $('#filesnav a').bind('click', function(){
        $('#filesnav a').removeClass('active');
        $('#filesnav a').parent().removeClass('active');

        $(this).addClass('active');
        $(this).parent().addClass('active');
        var currPage = $(this).attr('rel');
        var startItem = currPage * rowsShown;
        var endItem = startItem + rowsShown;
        $('#filesTable tbody tr').css('opacity','0.0').hide().slice(startItem, endItem).
        css('display','table-row').animate({opacity:1}, 300);
    });
}


function render_files_table(flag_month, first_select_month) {
    console.log("flag_month", flag_month, "window.first_select_month", first_select_month)
    if (flag_month == 1) {
        var month = first_select_month;
        flag_month = 0;
    }
    else {
        var month = $('#select_month option:selected').text();
    }
    //var month = $('#select_month option:selected').text();
    
    var data = new FormData();
    data.append("month", month)

    document.getElementById("processAllFiles").style.display="block";

    $.ajax({
        url: "http://127.0.0.1:3011/AllFiles",
        type: "POST",
        data: data,
        cache: false,
        dataType: "json",
        processData: false,
        contentType: false,
        success: function (json, status, jqXHR) {
            let arr = ["id_sbora", "id_file", "templ_name", "file_name", "file_date_update", "file_size"];
            table = document.getElementById("AllFilesTable");
            table.innerHTML = '';
            for (let i = 0; i < json.all_files_list.length; i++) {
                tr = document.createElement("tr");
                for (let j = 0; j < arr.length; j++) {
                    td = document.createElement("td");
                    td.innerHTML = json.all_files_list[i][arr[j]];
                    if (j == 3) {
                        td.id = "file_" + String(i)
                    }
                    tr.appendChild(td)
                }
                td = document.createElement("td");
                p = document.createElement("P");
                p.className = "btn btn btn-outline-success"
                p.id = "btn_file_" + String(i)
                p.setAttribute("onclick","downloadResFile(this.id);");
                p.innerHTML = "Скачать"
                td.appendChild(p)

                tr.appendChild(td)
                table.appendChild(tr)
            }
            pages();
            document.getElementById("processAllFiles").style.display = "none";
        },
        error: function (jqXHR, status, errorThrown) {
            console.log(status)
        },
    });
}

document.getElementById("select_month").addEventListener('change', function () {
    render_files_table(0, 0);
})