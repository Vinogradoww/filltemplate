from database import SessionLocal
from pydantic import BaseModel
from typing import Optional
import os


class Template(BaseModel):
    message: str

    class Config:
        orm_mode = True


class FileInput(BaseModel):
    filename: str
    exe_time: int
    size: int
    num_var: int        #количество переменных в шаблоне
    num_pass_var: int   #количество переданных переменных

    def prnt(self):
        print(self.filename, '\n', self.exe_time, '\n', self.size, '\n', self.num_var, '\n', self.num_pass_var)
    
    class Config:
        orm_mode = True


class TemplateOutput(BaseModel):
    name: str
    size: int
    num_files_used_this_template: int
    num_var: int
    avg_time: float

    class Config:
        orm_mode = True


class FileOutput(BaseModel):
    id: int
    id_sbora: int
    filename: str
    date: str
    timestart: str
    exe_time: int
    size: int
    json_name: str
    template_name: str
    num_var: int        
    num_pass_var: int 

    class Config:
        orm_mode = True


class FileOutput(BaseModel):
    id: int
    filename: str
    date: str
    timestart: str
    exe_time: int
    size: int
    json_name: str
    template_name: str
    num_var: int        
    num_pass_var: int 

    class Config:
        orm_mode = True


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Optional[str] = None


class User(BaseModel):
    id: int
    username: str
    disabled: Optional[bool] = None


class UserInDB(User):
    hashed_password: str